class AddDobAndLandMarkToUsers < ActiveRecord::Migration
  def change
    add_column :users, :dob, :timestamps
    add_column :users, :land_mark, :string
  end
end
